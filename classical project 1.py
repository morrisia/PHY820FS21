# -*- coding: utf-8 -*-
"""
Created on Sun Oct 17 02:03:45 2021

@author: Ian
"""
from ipywidgets import interact, fixed
import ipywidgets as widgets


#2.2 Implementation 

import numpy as np
from scipy.integrate import odeint

import matplotlib.pyplot as plt
#plt.rcParams.update({'font.size': 18})

def ode_rhs_exact(u_vec, t, *params):
    """ 
    Right-hand side (rhs) of the differential equation, with 
    u_vec = [\phi, \dot\phi] and params = [g, R].  Returns the list of
    d(u_vec)/dt, as prescribed by the differential equation.
    
    """
    phi, phidot = u_vec[0], u_vec[1]       # extract phi and phidot from the passed vector
    g, R = params[0], params[1]              # extract g and R from the passed parameters
    return [u_vec[1],-(g/R)*np.sin(phi)]    # extract phi and phidot from the passed vector


# parameters
g = 9.8  # in mks units
R = 5    # radius in meters

# absolute and relative tolerances for ode solver
abserr = 1.0e-8
relerr = 1.0e-6

# initial conditions for [phi, phidot]
phi0 = np.pi/180 * 20.  # convert initial phi to radians
u0_vec = [phi0, 0]

t_max = 15.  # integration time
t_pts = np.arange(0, t_max, 0.01)  # array of time points, spaced 0.01

# Integrate the differential equation and read off phi, phidot (note T!)
phi, phidot = odeint(ode_rhs_exact, u0_vec, t_pts, args=(g, R), 
                     atol=abserr, rtol=relerr).T

#Now plot your solutions 
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(t_pts, 180./np.pi * phi)
fig.tight_layout()  # make the spacing of subplots nicer

def solve_for_phi(phi0, phidot0=0, t_min=0., t_max=1., g=9.8, R=5.):
    """
    Solve the equation of motion for an ideal pendulum.
    The result for t, \phi(t) and \dot\phi(t) are returned for a range
    t_min < t < t_max and a hardwired (for now) time step of 0.01 seconds.
    The ODE solver is odeint from scipy, with specified tolerances. 
    Units are mks and angles are in radians.
    """

    # absolute and relative tolerances for ode solver
    abserr = 1.0e-8
    relerr = 1.0e-6

    # initial conditions for [phi, phidot]
    u0_vec = phi0,0

    t_pts = np.arange(t_min, t_max, 0.01)

    # Integrate the differential equation
    phi, phidot = odeint(ode_rhs_exact, u0_vec, t_pts, args=(g, R), 
                     atol=abserr, rtol=relerr).T
    
    return t_pts,phi ,phidot 

#Checking that the function works IE gives the same result as before 
phi0 = np.pi/180 * 20.  # convert initial phi to radians
t_pts, phi, phidot = solve_for_phi(phi0, t_max=15.)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(t_pts, 180./np.pi * phi)
fig.tight_layout()  # make the spacing of subplots nicer

#2.3.2 Implementation 
def ode_rhs_lo(u_vec, t, *params):
    """ 
    Right-hand side (rhs) of the EOM for the ideal pendulum, with 
    u_vec = [\phi, \dot\phi] and params = [g, R].  Returns the list of
    d(u_vec)/dt, as prescribed by the differential equation.
    
    """
    phi, phidot = u_vec[0], u_vec[1]
    a_1 = 1
    return [phidot, -g*(a_1*phi/R)]
   
def solve_for_phi_all(phi0, phidot0=0, t_min=0., t_max=1., g=9.8, R=5.):
    """
    Solve the EOM for the ideal pendulum using the exact equation and the 
    small-angle approximation. The results for t, \phi(t) and \dot\phi(t), 
    are returned for a grid with t_min < t < t_max and a hardwired (for now) 
    spacing of 0.01 seconds. The ODE solver is odeint from scipy, with specified 
    tolerances. Units are mks and angles are in radians.
    """

    # absolute and relative tolerances for ode solver
    abserr = 1.0e-8
    relerr = 1.0e-6

    # initial conditions for [phi, phidot]
    u0_vec = [phi0, phidot0]

    t_pts = np.arange(t_min, t_max, 0.01)

    # Integrate the differential equations
    phi, phidot = odeint(ode_rhs_exact, u0_vec, t_pts, args=(g,R), 
                     atol=abserr, rtol=relerr).T
    phi_lo, phidot_lo =  odeint(ode_rhs_lo, u0_vec, t_pts, args=(g,R), 
                     atol=abserr, rtol=relerr).T
    
    return t_pts, phi, phidot, phi_lo, phidot_lo

#Trying out the solutions 
    
phi0 = np.pi/180 * 20.
t_pts, phi, phidot, phi_lo, phidot_lo = solve_for_phi_all(phi0, t_max=15.)

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(1,1,1)
ax.plot(t_pts, 180./np.pi * phi)
ax.grid()
ax.plot(t_pts, 180./np.pi * phi_lo)
fig.tight_layout()  


#Here we see examples of applying limits to the x and y axes as well as labels and a title.
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(1,1,1)
ax.set_xlim(0.,15.)
ax.set_ylim(-25.,25.)
ax.set_xlabel('t (sec)')
ax.set_ylabel(r'$\phi$')
ax.set_title(r'$\phi_0 = 20$ degrees')
# Watch out: The empty space behind the comma matters, do not remove!!!
line_exact, = ax.plot(t_pts, 180./np.pi * phi, label='exact')
line_lo, = ax.plot(t_pts, 180./np.pi * phi_lo, label='LO')
ax.legend()

# save the figure
fig.savefig('p01_pendulum_solutions.png', bbox_inches='tight')

#Now, let's add some widgets 





#3. Adding Damping  
#Part 1
def ode_rhs_exact(u_vec, t, *params):
    """ 
    Right-hand side (rhs) of the differential equation, with 
    u_vec = [\phi, \dot\phi] and params = [g, R].  Returns the list of
    d(u_vec)/dt, as prescribed by the differential equation.
    
    """
    phi, phidot = u_vec[0], u_vec[1]       # extract phi and phidot from the passed vector
    g, R = params[0], params[1]              # extract g and R from the passed parameters
    b, m = params[2], params[3]
    return [ u_vec[1], -(g/R)*np.sin(phi)-(b/m)*phidot ]

def ode_rhs_lo(u_vec, t, g, R, b, m):
    """ 
    Right-hand side (rhs) of the EOM for the ideal pendulum, with 
    u_vec = [\phi, \dot\phi] and params = [g, R].  Returns the list of
    d(u_vec)/dt, as prescribed by the differential equation.
    
    """
    phi, phidot = u_vec[0], u_vec[1]
    c1 = 1.
    c2 = b/m
    return [phidot, -g*(c1*phi/R)-c2*phidot] # second order phi derivative 

def solve_for_phi_all(phi0, phidot0=0, t_min=0., t_max=1., g=9.8, R=5., b=.7, m=2.):
    
    # absolute and relative tolerances for ode solver
    abserr = 1.0e-8
    relerr = 1.0e-6

    # initial conditions for [phi, phidot]
    u0_vec = [phi0, phidot0]

    t_pts = np.arange(t_min, t_max, 0.01)

    # Integrate the differential equations
    phi3, phidot3 = odeint(ode_rhs_exact, u0_vec, t_pts, args=(g,R,b,m), 
                     atol=abserr, rtol=relerr).T
    phi_1o3, phidot_lo3 =  odeint(ode_rhs_lo, u0_vec, t_pts, args=(g,R,b,m), 
                     atol=abserr, rtol=relerr).T
    
    return t_pts, phi3, phidot3


#Trying it out 
phi3 = np.pi/180 * 10.
t_pts, phi3, phidot3 = solve_for_phi_all(phi3, t_max=15.)

#plotting
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(1,1,1)
ax.set_xlim(0.,15.)
ax.set_ylim(-25.,25.)
ax.set_xlabel('t (sec)')
ax.set_ylabel(r'$\phi$')
ax.set_title(r'$\phi_0 = 20$ degrees')
line_exact3, = ax.plot(t_pts, 180./np.pi * phi3, label='exact')
ax.legend()
ax.grid()

# save the figure
fig.savefig('p01_pendulum_solutions.png')







